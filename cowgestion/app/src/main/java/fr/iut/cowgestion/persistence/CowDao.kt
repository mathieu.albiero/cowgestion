package fr.iut.cowgestion.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.iut.cowgestion.model.Cow

@Dao
interface CowDao {

    @Query("SELECT * FROM cows")
    fun getAllCows(): LiveData<List<Cow>>

    @Query("SELECT * FROM cows WHERE numBoucle = (:numBoucle)")
    fun getCow(numBoucle: Long): LiveData<Cow>

    @Query("SELECT * FROM cows WHERE name LIKE (:name) LIMIT 1")
    fun findByName(name: String): List<Cow>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createCow(cow: Cow)

    @Update
    fun updateCow(cow: Cow)

    @Query("DELETE FROM cows WHERE (:numBoucle) = numBoucle")
    fun deleteCow(numBoucle: Long)

    @Query("DELETE FROM cows")
    suspend fun deleteAll()


}