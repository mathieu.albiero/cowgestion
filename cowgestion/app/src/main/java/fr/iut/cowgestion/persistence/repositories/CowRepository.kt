package fr.iut.cowgestion.persistence.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.persistence.CowDao

class CowRepository (private val cowDao: CowDao){

    val allCows: LiveData<List<Cow>> = cowDao.getAllCows()

    fun insert(cow: Cow) {
        cowDao.createCow(cow)
        Log.d("INSERTION:", "INSERT COW")
    }

    fun update(cow: Cow) {
        cowDao.updateCow(cow)
        Log.d("UPDATE:", "UPDATE COW")
    }

    fun delete(numBoucle: Long){
        cowDao.deleteCow(numBoucle)
        Log.d("DELETE:", "DELETE COW")
    }

    suspend fun deleteAll() {
        cowDao.deleteAll()
        Log.d("INSERTION:", "DELETE ALL FROM cows")
    }

    fun findCow(numBoucle: Long): LiveData<Cow>{
        return cowDao.getCow(numBoucle)
    }


}