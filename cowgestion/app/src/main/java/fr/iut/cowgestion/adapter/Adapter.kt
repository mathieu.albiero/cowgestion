package fr.iut.cowgestion.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.iut.cowgestion.R
import fr.iut.cowgestion.model.Cow

class Adapter: ListAdapter<Cow, Adapter.CowViewHolder>(CowsComparator()) {

    private lateinit var listener : onItemClickListener

    interface onItemClickListener{
        fun onItemClick(numBoucle: Long)
    }

    fun setOnItemClickListener(listener : onItemClickListener){
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CowViewHolder {
        return CowViewHolder.create(parent, listener)
    }

    override fun onBindViewHolder(holder: CowViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.numBoucle, current.name)
    }

    class CowViewHolder(itemView: View, listener: onItemClickListener) : RecyclerView.ViewHolder(itemView) {
        private val numBoucle = itemView.findViewById<TextView>(R.id.numBoucle)
        private val name = itemView.findViewById<TextView>(R.id.nomVache)
        private val gender = itemView.findViewById<Spinner>(R.id.gender_spinner)

        var cow: Cow? = null

        init {
            itemView.setOnClickListener {
                cow?.let { listener.onItemClick(it.numBoucle) }
            }
        }

        fun bind(_numBoucle: Long, _name: String) {
            cow = Cow(_numBoucle, _name)
            numBoucle.text = _numBoucle.toString()
            name.text = _name

        }

        companion object {
            fun create(parent: ViewGroup, listener: onItemClickListener): CowViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_cow, parent, false)
                return CowViewHolder(view, listener)
            }
        }
    }

    class CowsComparator : DiffUtil.ItemCallback<Cow>() {
        override fun areItemsTheSame(oldItem: Cow, newItem: Cow): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Cow, newItem: Cow): Boolean {
            return oldItem.numBoucle == newItem.numBoucle && oldItem.name == newItem.name
        }
    }
}