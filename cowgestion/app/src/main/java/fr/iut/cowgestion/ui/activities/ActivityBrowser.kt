package fr.iut.cowgestion.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import fr.iut.cowgestion.R


class ActivityBrowser: AppCompatActivity() {

    private lateinit var editTextToken : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)

        editTextToken = findViewById(R.id.edit_text_token)

        setSupportActionBar(findViewById(R.id.browser_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val wb = findViewById<View>(R.id.my_web_view) as WebView

        wb.loadUrl("https://www.30boxes.com/api/api.php?"
                + "method=user.Authorize"
                + "&apiKey=8618647-befbdb6c30a8de5b2884947054ae7a71"
                + "&applicationName=CowGestion"
                + "&applicationLogoUrl=http%3A%2F%2F83degrees.com%2F83degrees.gif")
    }

    fun validateToken(view: View) {
        val data = Intent()
        if(editTextToken.text.isBlank()) {
            setResult(RESULT_CANCELED, data)
        }
        else {
            data.putExtra("token", editTextToken.text)
            setResult(RESULT_OK, data)
        }
        finish()
    }

}