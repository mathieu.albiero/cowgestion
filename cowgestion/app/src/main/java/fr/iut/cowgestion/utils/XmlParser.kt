package fr.iut.cowgestion.utils

import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream

private val ns: String? = null

class XmlParser {

    // Class qui était utilisé pour récuprer le pong en fesant un ping sur l'api

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): Response {
        inputStream.use {
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(it, null)
            parser.nextTag()
            return readRsp(parser)
        }
    }

    data class Response(val ping: String?, val msg: String?)

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readRsp(parser: XmlPullParser): Response {
        parser.require(XmlPullParser.START_TAG, ns, "rsp")
        var ping: String? = null
        var msg: String? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "ping" -> ping = readPing(parser)
                "msg" -> msg = readMsg(parser)
                else -> skip(parser)
            }
        }
        return Response(ping, msg)
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readPing(parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, ns, "ping")
        val ping = readText(parser)
        parser.require(XmlPullParser.END_TAG, ns, "ping")
        return ping
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readMsg(parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, ns, "msg")
        val msg = readText(parser)
        parser.require(XmlPullParser.END_TAG, ns, "msg")
        return msg
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readText(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }


}