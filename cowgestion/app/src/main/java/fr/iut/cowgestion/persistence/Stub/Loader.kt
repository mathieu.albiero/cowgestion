package fr.iut.cowgestion.persistence.Stub

import fr.iut.cowgestion.model.Cow

interface Loader {
    fun load():List<Cow>
}