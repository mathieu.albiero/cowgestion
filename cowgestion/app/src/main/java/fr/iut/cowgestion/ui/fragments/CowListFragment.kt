package fr.iut.cowgestion.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.iut.cowgestion.R
import fr.iut.cowgestion.adapter.Adapter
import fr.iut.cowgestion.persistence.CowListViewModel


class CowListFragment: Fragment() {
    private lateinit var cowListViewModel: CowListViewModel

    private var listener : OnInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.cow_list_fragment, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.cow_list)
        val adapter = Adapter()

        setHasOptionsMenu(true)

        cowListViewModel = loadVM()
        cowListViewModel.allCows.observe(viewLifecycleOwner, Observer { cows ->
            cows?.let { adapter.submitList(it) }
        })
        recyclerView.adapter = adapter
        view.findViewById<FloatingActionButton>(R.id.floating_btn).setOnClickListener { addNewCow() }
        adapter.setOnItemClickListener(object : Adapter.onItemClickListener{
            override fun onItemClick(numBoucle: Long) {
                onClickCow(numBoucle)
            }

        })
        return view
    }

    private fun addNewCow() {
        listener?.onAddNewCow()
    }

    private fun loadVM(): CowListViewModel = listener?.onLoadVM()!!

    private fun onClickCow(numBoucle: Long){
        listener?.onClickCow(numBoucle)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_login -> {
                log()
                true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun log(){
        listener?.onLog()
    }

    interface OnInteractionListener {
        fun onAddNewCow()
        fun onLoadVM(): CowListViewModel
        fun onClickCow(numBoucle: Long)
        fun onLog()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnInteractionListener
    }
}