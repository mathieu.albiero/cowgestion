package fr.iut.cowgestion.persistence

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.persistence.repositories.CowRepository

class CowListViewModel(private val repository: CowRepository) : ViewModel() {

    val allCows: LiveData<List<Cow>> = repository.allCows
}

class CowListViewModelFactory(private val repository: CowRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CowListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CowListViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

