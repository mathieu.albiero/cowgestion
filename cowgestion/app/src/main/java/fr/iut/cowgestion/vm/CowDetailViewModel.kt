package fr.iut.cowgestion.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.persistence.repositories.CowRepository
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CowDetailViewModel(private val repository: CowRepository): ViewModel() {

    fun insert(cow: Cow) = viewModelScope.launch(Dispatchers.IO + CoroutineName("DBInsertion")) {
        repository.insert(cow)
    }

    fun update(cow: Cow) = viewModelScope.launch(Dispatchers.IO + CoroutineName("DBInsertion")) {
        repository.update(cow)
    }

    fun findCow(numBoucle: Long): LiveData<Cow> {
        return repository.findCow(numBoucle)
    }

    fun delete(numBoucle: Long) = viewModelScope.launch(Dispatchers.IO + CoroutineName("DBInsertion")) {
        repository.delete(numBoucle)
    }
}

class CowDetailViewModelFactory(private val repository: CowRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CowDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CowDetailViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
