package fr.iut.cowgestion

import android.app.Application
import fr.iut.cowgestion.persistence.db.CowDatabase
import fr.iut.cowgestion.persistence.repositories.CowRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class CowsApplication : Application() {

    companion object{
        var token: String = ""
    }

    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { CowDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { CowRepository(database.cowDao()) }

}