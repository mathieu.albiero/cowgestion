package fr.iut.cowgestion.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import fr.iut.cowgestion.CowsApplication
import fr.iut.cowgestion.R
import fr.iut.cowgestion.persistence.CowListViewModel
import fr.iut.cowgestion.persistence.CowListViewModelFactory
import fr.iut.cowgestion.ui.fragments.CowListFragment

class MainActivity : AppCompatActivity(), CowListFragment.OnInteractionListener {

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            handleToken(data)
        }
        else {
            Toast.makeText(this, "Token invalide", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(supportFragmentManager.findFragmentById(R.id.my_fragment) == null){
            supportFragmentManager.beginTransaction()
                .add(R.id.my_fragment, CowListFragment())
                .commit()
        }
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.my_toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_master_menu,menu)
        return true
    }

    override fun onAddNewCow() {
        startActivity(NewCowActivty.getIntent(this, 0L))
    }

    override fun onLoadVM(): CowListViewModel {
        val cowViewModel: CowListViewModel by viewModels {
            CowListViewModelFactory ((application as CowsApplication).repository)
        }
        return cowViewModel
    }

    override fun onClickCow(numBoucle: Long) {
        startActivity(NewCowActivty.getIntent(this, numBoucle))
    }

    override fun onLog() {
        resultLauncher.launch(Intent(this, ActivityBrowser::class.java))
    }

    private fun handleToken(data: Intent?) {
        try {
            CowsApplication.token = data?.extras?.get("token").toString()
            Toast.makeText(this, "Le token a été validé", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(this, "Un problème est survenu", Toast.LENGTH_SHORT).show()
        }
    }

}