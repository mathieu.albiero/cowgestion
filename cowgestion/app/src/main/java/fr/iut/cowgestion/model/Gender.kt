package fr.iut.cowgestion.model

enum class Gender {
    FEMELLE,
    MALE,
    INCONNU
}