package fr.iut.cowgestion.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CalendarView
import android.widget.EditText
import android.widget.Spinner
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.iut.cowgestion.R
import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.model.Gender
import fr.iut.cowgestion.utils.DateConverter
import fr.iut.cowgestion.vm.CowDetailViewModel


class CowFragment: Fragment() {

    companion object{
        private const val EXTRA_COW_NUM_BOUCLE = "fr.iut.cowgestion.extra_num_boucle_cow"

        fun newInstance(numBoucle: Long) = CowFragment().apply {
            arguments = bundleOf(EXTRA_COW_NUM_BOUCLE to numBoucle)
        }
    }

    private var listener : OnInteractionListener? = null

    private lateinit var cowDetailViewModel: CowDetailViewModel

    private val dateConverter = DateConverter()

    private lateinit var cow: Cow
    private var cowNumBoucle : Long = 0

    private lateinit var editTextNumBoucle: EditText
    private lateinit var editTextName: EditText

    private lateinit var calendarViewBirth : CalendarView

    private lateinit var calendarCalvingDate : CalendarView

    private lateinit var spinnerGender: Spinner

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.cow_fragment, container, false)

        setHasOptionsMenu(true)

        editTextName = view.findViewById(R.id.edit_nom_vache)
        editTextNumBoucle = view.findViewById(R.id.edit_num_boucle)

        spinnerGender = view.findViewById(R.id.gender_spinner)

        calendarViewBirth = view.findViewById(R.id.birthdate)

        calendarCalvingDate = view.findViewById(R.id.calving_date)

        calendarViewBirth.setOnDateChangeListener { calendarView, year, month, day -> cow.birthDate = dateConverter.getRightDate(year, month, day)}

        calendarCalvingDate.setOnDateChangeListener { calendarView, year, month, day -> saveCalvingDate(year, month, day) }

        cowDetailViewModel = loadVM()
        cowNumBoucle = arguments?.getLong(EXTRA_COW_NUM_BOUCLE)!!

        val adapter: ArrayAdapter<Gender> = initArrayAdapater()
        spinnerGender.adapter = adapter

        if(cowNumBoucle == 0L){
            cow = Cow()
        }
        else {
            cowDetailViewModel.findCow(cowNumBoucle).observe(viewLifecycleOwner, Observer {
                cow = it
                editTextName.setText(it.name)
                editTextNumBoucle.setText(it.numBoucle.toString())
                spinnerGender.setSelection(it.gender.ordinal)
                calendarViewBirth.date = it.birthDate?.time!!
            })
        }

        return view
    }

    private fun saveCalvingDate(year: Int, month: Int, day: Int) {
        listener?.OnSaveCalvingDate(year, month, day, cow.numBoucle)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_add->{
                if(cow.numBoucle == 0L) {
                    saveCow()
                    true
                }
                else {
                    updateCow()
                }
            }
            R.id.action_remove->{
                if(cow.numBoucle != 0L) {
                    deleteCow()
                    true
                }
                else{
                    false
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteCow() {
        cowDetailViewModel.delete(cow.numBoucle)
        listener?.onCowsChanged()
    }

    private fun updateCow() {
        if(editTextNumBoucle.text.isNotBlank() && editTextName.text.isNotBlank()) {
            cow.numBoucle = editTextNumBoucle.text.toString().toLong()
            cow.name = editTextName.text.toString()
            cow.gender = Gender.valueOf(spinnerGender.selectedItem.toString())
        }
        cowDetailViewModel.update(cow)
        listener?.onCowsChanged()
    }

    private fun saveCow() {
        if(editTextNumBoucle.text.isNotBlank() && editTextName.text.isNotBlank()) {
            cow.numBoucle = editTextNumBoucle.text.toString().toLong()
            cow.name = editTextName.text.toString()
            cow.gender = Gender.valueOf(spinnerGender.selectedItem.toString())
            cowDetailViewModel.insert(cow)
            listener?.onCowsChanged()
        }
        else {
            listener?.OnNumBoucleInvalid()
        }
    }

    private fun initArrayAdapater() = listener?.onInitArrayAdapater()!!

    interface OnInteractionListener {
        fun onLoadVM(): CowDetailViewModel
        fun onCowsChanged()
        fun OnNumBoucleInvalid()
        fun onInitArrayAdapater(): ArrayAdapter<Gender>
        fun OnSaveCalvingDate(year: Int, month: Int, day: Int, cowNumBoucle: Long)
    }

    private fun loadVM(): CowDetailViewModel {
        return listener?.onLoadVM()!!
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnInteractionListener
    }
}