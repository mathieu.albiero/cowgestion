package fr.iut.cowgestion.utils

import java.text.SimpleDateFormat
import java.util.*

class DateConverter {

    fun getRightDate(year: Int, month: Int, day: Int): Date?{
        var tmp : Int
        var correctMonth: String
        var correctDay: String
        if((month + 1) < 10){
            tmp = month + 1
            correctMonth = "0$tmp"

        }
        else {
            correctMonth = (month + 1).toString()
        }
        if(day < 10){
            correctDay = "0$day"
        }
        else {
            correctDay = day.toString()
        }
        return SimpleDateFormat("yyyy/MM/dd").parse("$year/$correctMonth/$correctDay")
    }
}