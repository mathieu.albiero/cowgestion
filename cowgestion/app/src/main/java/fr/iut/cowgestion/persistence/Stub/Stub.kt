package fr.iut.cowgestion.persistence.Stub

import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.model.Gender

class Stub: Loader {

    override fun load(): List<Cow> {
        val lesVaches = ArrayList<Cow>()
        lesVaches.add(Cow(numBoucle = 123 , name = "Margueritte" , age = 4, gender = Gender.FEMELLE))
        lesVaches.add(Cow(numBoucle = 562 , name = "Violette" , age = 9, gender = Gender.FEMELLE))
        lesVaches.add(Cow(numBoucle = 785 , name = "Pipoune" , age = 1, gender = Gender.FEMELLE))
        lesVaches.add(Cow(numBoucle = 635 , name = "Cascade" , age = 6, gender = Gender.FEMELLE))
        lesVaches.add(Cow(numBoucle = 253 , name = "Gertrude" , age = 10, gender = Gender.FEMELLE))
        lesVaches.add(Cow(numBoucle = 986 , name = "Malo" , age = 8, gender = Gender.MALE))

        return lesVaches
    }
}