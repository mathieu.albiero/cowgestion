package fr.iut.cowgestion.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import fr.iut.cowgestion.CowsApplication
import fr.iut.cowgestion.R
import fr.iut.cowgestion.model.Gender
import fr.iut.cowgestion.ui.fragments.CowFragment
import fr.iut.cowgestion.vm.CowDetailViewModel
import fr.iut.cowgestion.vm.CowDetailViewModelFactory

class NewCowActivty: AppCompatActivity(), CowFragment.OnInteractionListener{

    private var mainMenu: Menu? = null

    companion object {
        private const val EXTRA_COW_NUM_BOUCLE = "fr.iut.cowgestion.extra_num_boucle_cow"

        fun getIntent(context: Context, cowNumBoucle: Long) =
            Intent(context, NewCowActivty::class.java).apply {
                putExtra(EXTRA_COW_NUM_BOUCLE, cowNumBoucle)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportFragmentManager.findFragmentById(R.id.my_fragment) == null) {
            supportFragmentManager.beginTransaction()
                .add(
                    R.id.my_fragment,
                    CowFragment.newInstance(intent.getLongExtra(EXTRA_COW_NUM_BOUCLE, 123L))
                )
                .commit()
        }
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        mainMenu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onLoadVM(): CowDetailViewModel {
        val cowDetailViewModel: CowDetailViewModel by viewModels {
            CowDetailViewModelFactory ((application as CowsApplication).repository)
        }
        return cowDetailViewModel
    }

    override fun onCowsChanged() = finish()

    override fun OnNumBoucleInvalid() {
        Toast.makeText(this, "Information(s) invalide", Toast.LENGTH_SHORT).show()
    }



    override fun onInitArrayAdapater() = ArrayAdapter(this, android.R.layout.simple_spinner_item, Gender.values())

    override fun OnSaveCalvingDate(year: Int, month: Int, day: Int, cowNumBoucle: Long) {
        if(CowsApplication.token.isNotBlank()) {
            if(cowNumBoucle > 0L) {
                val tmpMonth = month + 1
                val queue = Volley.newRequestQueue(this)
                val url = "https://www.30boxes.com/api/api.php?method=events.AddByElements" +
                        "&apiKey=8618647-befbdb6c30a8de5b2884947054ae7a71" +
                        "&authorizedUserToken=" + CowsApplication.token +
                        "&summary=velage+du+numero+" + cowNumBoucle +
                        "&dateStart=" + tmpMonth + "/" + day + "/" + year

                val stringRequest = StringRequest(
                    Request.Method.POST, url, { response ->
                        Toast.makeText(this, "Date de vêlage ajoutée", Toast.LENGTH_SHORT).show()

                    }, {
                        Toast.makeText(this, "Un problême est survenu", Toast.LENGTH_SHORT).show()
                    })

                queue.add(stringRequest)
            }
            else {
                Toast.makeText(this, "Numero de boucle invalide", Toast.LENGTH_SHORT).show()
            }

        }
        else {
            Toast.makeText(this, "Connectez vous avant de pouvoir ajouter une date", Toast.LENGTH_SHORT).show()
        }
    }
}