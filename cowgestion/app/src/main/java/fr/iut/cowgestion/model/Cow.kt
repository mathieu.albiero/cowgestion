package fr.iut.cowgestion.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "cows")
data class Cow (
    @PrimaryKey var numBoucle: Long = 0L,
    var name: String = "",
    var age: Int = 0,
    var gender: Gender = Gender.INCONNU,
    var birthDate : Date? = null
){
}