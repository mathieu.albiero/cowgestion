package fr.iut.cowgestion.persistence.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.iut.cowgestion.model.Cow
import fr.iut.cowgestion.model.Gender
import fr.iut.cowgestion.persistence.CowDao
import fr.iut.cowgestion.utils.DateToLongConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

@Database(entities = [Cow::class], version = 1)
@TypeConverters(DateToLongConverter::class)
abstract class CowDatabase: RoomDatabase()  {

    abstract fun cowDao(): CowDao

    companion object {

        // Singleton pour éviter l'ouverture de plusieurs instances de la base de données.
        @Volatile
        private var INSTANCE: CowDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): CowDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CowDatabase::class.java,
                    "cow_database"
                ).addCallback(CowDatabaseCallback(scope)).build()
                INSTANCE = instance

                instance
            }
        }
    }


    class CowDatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.cowDao())
                }
            }
        }

        suspend fun populateDatabase(cowDao: CowDao) {
            // Suppression du contenu de la table cows.
            cowDao.deleteAll()

            // Ajout de vache.
            var cow = Cow(numBoucle = 123 , name = "Margueritte" , age = 4, birthDate = Date(1267498779875), gender = Gender.FEMELLE)
            cowDao.createCow(cow)
            cow = Cow(numBoucle = 986 , name = "Malo" , age = 8, birthDate = Date(1234345678908), gender = Gender.MALE)
            cowDao.createCow(cow)
        }
    }
}